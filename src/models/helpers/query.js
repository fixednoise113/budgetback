export const create_query = (table, body, required) => {
    let insert_string = ""
    let values = ""
    for (const field in body) {
        if (field in required && body[field]) {
            insert_string = insert_string + field + ","
            values = values + "?,"
        } else {
            throw field.charAt(0).toUpperCase() + field.slice(1) + " is required"
        }
    }
    insert_string.slice(-1)
    values.slice(-1)
    return "INSERT INTO " + table + "(" + insert_string + ") VALUES(" + values + ")" 
}

export const update_query = (table, body, fields) => {
    let update_params = ""
    for (const field in body) {
        if (field in fields) {
            update_params = update_params + field + " = ?,"
        }
    }
    update_params.slice(-1)
    return "UPDATE " + table + " SET ("+ update_params +")"
}

export const get_query = (table, id, fields, hidden) => {
    let selected = ""
    fields.forEach(el => {
        selected = el in hidden ? '' : selected + el + ','
    })
    return `SELECT ${selected} FROM ${table} ${id ? 'WHERE id = ?' : ''}`
}

export const delete_query = (table) => {
    return "DELETE FROM " + table + " WHERE id = ?"
}