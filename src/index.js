import express from "express"
import rpc from "./router/rpc" 

const app = express()

app.use(express.urlencoded({extended: true}))
app.use(express.json())

app.post('/v1/api', function (req, res) {
    const data = rpc(req.body)
    res.status(data.code).json(data.response)
})
app.get('/*', function (req, res) {
    res.status(404).send("Get method is not supported for this service")
})

app.listen("8087", () => {
     console.log("start listen port 8087")
})