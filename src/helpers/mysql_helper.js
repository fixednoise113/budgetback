import mysql from "mysql"
import validator from "./shield"

const client = mysql.createConnection({
    user:"user",
    password: "password",
    host: "localhost",
    database: "budget"
})

const query = (query, values = null) => {
    values = values ? validator(values) : []

    if (typeof query !== 'string') {
        throw "error format"
    }
    client.connect()

    client.query(query, values, (error, rows, fields) => {
        if (error) {
            throw error
        }
        return rows
    })

    client.end()
}

export default query