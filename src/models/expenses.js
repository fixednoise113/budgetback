import dbQuery from "../helpers/mysql_helper"
import { create_query, update_query, get_query, delete_query } from "./helpers/query"
const fields = ['id', 'title', 'description', 'price', 'user_id']
const hidden = ['user_id']
const required = ['title', 'description', 'price', 'user_id']
const table = "expenses"

export default {
    store: (body) => {
        const query = create_query(table, body, required)
        return dbQuery(query, body)
    },
    update: (body) => {
        const query = update_query(table, body)
        return dbQuery(query, body)
    },
    get: (id = null) => {
        const query = get_query(table,id, fields, hidden)
        return {code: 200, body: slice_hidden_fields(dbQuery(query, id))}
    },
    destroy: (id) => {
        const query = delete_query(table)
        return {code: 204, body: dbQuery(query, id)}
    }
}