import actions from "./actions/actions"

const rpc = (json) => {
    if (!json.method || !actions[json.method]) {
        return {
            code: 404,
            response: "method was not found"
        }
    }

    const res = actions(json.method, json.params)

    return {
        code: res.code,
        response: response.body
    }

}

export default rpc