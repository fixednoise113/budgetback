import dbQuery from "../helpers/mysql_helper"
import { create_query, update_query, get_query, delete_query } from "./helpers/query"


const fields = ['id', 'name', 'email', 'password']
const hidden = ['password']
const required = ['name', 'email', 'password']
const table = "users"

export default {
    store: (body) => {
        const query = create_query(table,body, required)
        return {code: 201, body: slice_hidden_fields(dbQuery(query, body))}
    },
    update: (body) => {
        const query = update_query(table,body)
        return {code:200, body: slice_hidden_fields(dbQuery(query, body))}
    },
    get: (id = null) => {
        const query = get_query(table,id, fields, hidden)
        return {code: 200, body: slice_hidden_fields(dbQuery(query, id))}
    },
    destroy: (id) => {
        const query = delete_query(table)
        return {code: 204, body: dbQuery(query, id)}
    }
}