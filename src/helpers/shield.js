export default function(values) {
    if (typeof values === "object") {
        values = values.values()
    }
    if (typeof values !== "array") {
        throw "shield variable must be array or object"
    }
    const vals = []

    values.forEach(element => {
        vals.push(msql.escape(element))
    })

    return vals
}