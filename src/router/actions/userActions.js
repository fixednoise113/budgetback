import userModel from "../../models/user"

const user = {
    createUser: (body) => {
        return userModel.store(body)
    },
    getUsers: (id = null) => {
        console.log('test')
        return userModel.get(id)
    },
    updateUser: (body) => {
        return userModel.update(body)
    },
    deleteUser: (id) => {
        return userModel.destroy(id)
    }
}

export default user